import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-rule-form',
  templateUrl: './rule-form.component.html',
  styleUrls: ['./rule-form.component.css'],
})
export class RuleFormComponent implements OnInit {
  constructor() {}


  ngOnInit(): void {

  }

  providerFoldersList: string[] = [
    'Folder A',
    'Folder B',
    'Folder C',
    'Folder D',
    'Folder E',
    'Folder F',
  ];
}
