import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-rule-menu',
  templateUrl: './rule-menu.component.html',
  styleUrls: ['./rule-menu.component.css']
})
export class RuleMenuComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  step = 0;

  setStep(index: number) {
    this.step = index;
  }

  nextStep() {
    this.step++;
  }

  prevStep() {
    this.step--;
  }
}
