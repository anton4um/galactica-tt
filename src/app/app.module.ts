import { MaterialModule } from './material-module';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { RulesListComponent } from './rules-list/rules-list.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RuleMenuComponent } from './rule-menu/rule-menu.component';
import { RuleFormComponent } from './rule-form/rule-form.component';
import { RuleFormActionsComponent } from './rule-form-actions/rule-form-actions.component';

@NgModule({
  declarations: [
    AppComponent,
    RulesListComponent,
    RuleMenuComponent,
    RuleFormComponent,
    RuleFormActionsComponent
  ],
  imports: [
    BrowserAnimationsModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
